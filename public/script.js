function setLocation(curLoc) {
    history.pushState(null, null, curLoc);
    return;
}

$('#myModal').on('hide.bs.modal', function (e) {
    if (location.hash == '#modal') window.history.back();
});

$(window).on('popstate', function (event) {
    if (event.state !== null) $('.modal').modal('hide');
});

$('.modal').on('shown.bs.modal', function () {
    document.getElementById('firstname').value = localStorage.getItem('firstname');
    document.getElementById('email').value = localStorage.getItem('email');
    document.getElementById('comment').value = localStorage.getItem('comment');
    if (localStorage.getItem('check') === "true") {
        $('#check').prop('checked', true);
    }
    else { $('#check').prop('checked', false); }
});

$('.modal').on('hidden.bs.modal', function () {
    localStorage.setItem('firstname', $('#firstname').val());
    localStorage.setItem('email', $('#email').val());
    localStorage.setItem('comment', document.getElementById('comment').value);
    localStorage.setItem('check', $('#check').is(':checked'));
});

function done() {
    document.getElementById('firstname').value = "";
    document.getElementById('email').value = "";
    document.getElementById('comment').value = "";
    $('#check').prop('checked', false);
    localStorage.clear();
};

$(document).ready(function () {
    $('#formData').on('submit', function (e) {
        e.preventDefault();
                var href = $(this).attr("action");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: href,
            data: $(this).serialize(),
            success: function(response){
                if(response.status == "success"){
                    alert("We received your submission, thank you!");
                }else{
                    alert("An error occured: " + response.message);
                }
            }
        });
    });
});
